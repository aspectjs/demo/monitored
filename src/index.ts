import { getWeaver } from "@aspectjs/core";
import { Monitored } from "./annotations/monitored.annotation";
import { MonitoredAspect } from "./aspects/monitored.aspect";

getWeaver().enable(new MonitoredAspect());
class UserResource {
  @Monitored()
  fetchUsers() {
    // simutate an http fetch with some delay
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve(["Joe", "Jack", "William", "Avrell"]);
      }, 500);
    });
  }

  @Monitored()
  deleteUser(name: string) {
    // simutate an http fetch with some delay
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        reject([`user ${name} does not exist`]);
      }, 400);
    });
  }
}
async function main() {
  const userResource = new UserResource();

  console.log(`${await userResource.fetchUsers()}`);
  console.log(`${await userResource.deleteUser("John")}`);
}

main();
