import { AnnotationType } from "@aspectjs/common";
import { ANNOTATION_FACTORY } from "./annotation-factory";

export const Monitored = ANNOTATION_FACTORY.create(
  AnnotationType.METHOD,
  function Monitored() {}
);
