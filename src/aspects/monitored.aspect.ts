import {
  Around,
  AroundContext,
  Aspect,
  JoinPoint,
  PointcutType,
  on,
} from "@aspectjs/core";
import { Monitored } from "../annotations/monitored.annotation";

@Aspect()
export class MonitoredAspect {
  constructor(
    private readonly logFn: (message: string) => void = console.log
  ) {}

  @Around(on.methods.withAnnotations(Monitored))
  logTime(
    ctxt: AroundContext<PointcutType.METHOD>,
    jp: JoinPoint,
    jpArgs: unknown[]
  ) {
    const startTime = new Date().getTime();
    try {
      const returnedValue = jp(...jpArgs);
      if (returnedValue instanceof Promise) {
        return this.handlePromise(ctxt, returnedValue, startTime);
      }

      return this.handleReturn(ctxt, returnedValue, startTime);
    } catch (error) {
      return this.handleError(ctxt, error, startTime);
    }
  }
  protected handleError(
    ctxt: AroundContext<PointcutType.METHOD, object>,
    error: any,
    startTime: number
  ) {
    this.logFn(
      `${ctxt.target} errored after ${new Date().getTime() - startTime} ms`
    );

    // throw the original error
    throw error;
  }
  protected handleReturn(
    ctxt: AroundContext<PointcutType.METHOD, object>,
    returnedValue: unknown,
    startTime: number
  ) {
    this.logFn(
      `${ctxt.target} returned after ${new Date().getTime() - startTime} ms`
    );
    return returnedValue;
  }

  protected handlePromise(
    ctxt: AroundContext<PointcutType.METHOD>,
    returnedValue: Promise<unknown>,
    startTime: number
  ) {
    // return a new promise that logs the execution time
    return returnedValue.then(
      (r) => {
        this.logFn(
          `${ctxt.target} resolved after ${new Date().getTime() - startTime} ms`
        );
        return r;
      },
      (r) => {
        this.logFn(
          `${ctxt.target} rejected after ${new Date().getTime() - startTime} ms`
        );
        return r;
      }
    );
  }
}
